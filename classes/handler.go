package classes

import (
	"github.com/gofiber/fiber"
	"go-fitness/classes/entities"
	classErrors "go-fitness/classes/errors"
	"go-fitness/common"
	"go-fitness/common/errors"
	"go-fitness/storage"
	"net/http"
)

type ClassHandler struct {
	storage storage.Storage
}

func New(s storage.Storage) *ClassHandler {
	return &ClassHandler{
		storage: s,
	}
}

func (ch *ClassHandler) Create() func(c *fiber.Ctx) {
	return func(c *fiber.Ctx) {
		class := c.Locals(common.BodyLocalsKey).(*entities.Class)

		// perform save
		if err := ch.storage.SaveClass(class); err != nil {
			switch e := err.(type) {
			case *classErrors.ClassCreationError:
				c.Status(http.StatusBadRequest)

				_ = c.JSON(common.ErrorResponse{
					Message: e.Message,
				})
			default:
				c.Status(http.StatusInternalServerError)

				_ = c.JSON(common.ErrorResponse{
					Message: errors.HttpServerInternalError,
					Details: err.Error(),
				})
			}

			return
		}

		c.SendStatus(http.StatusCreated)
	}
}
