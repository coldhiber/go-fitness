package entities

import (
	"cloud.google.com/go/civil"
	constants "go-fitness/classes/errors"

	"go-fitness/common/errors"
)

type Class struct {
	Name      string     `json:"name"`
	StartDate civil.Date `json:"startDate"`
	EndDate   civil.Date `json:"endDate"`
	Capacity  int        `json:"capacity"`
}

func (c *Class) Validate() error {
	if c.Name == "" {
		return &errors.ValidationError{Message: constants.NameIsEmpty}
	}

	if c.StartDate.Year == 0 {
		return &errors.ValidationError{Message: constants.InvalidStartDate}
	}

	if c.EndDate.Year == 0 {
		return &errors.ValidationError{Message: constants.InvalidEndDate}
	}

	if c.StartDate.After(c.EndDate) {
		return &errors.ValidationError{Message: constants.EndDateBeforeStart}
	}

	if c.Capacity == 0 || c.Capacity < 0 {
		return &errors.ValidationError{Message: constants.InvalidCapacity}
	}

	return nil
}
