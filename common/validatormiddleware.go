package common

import (
	"encoding/json"
	"github.com/gofiber/fiber"
	"go-fitness/common/errors"
	"net/http"
)

const BodyLocalsKey = "body"

type Validator interface {
	Validate() error
}

func UnmarshalAndValidateMiddleware(obj interface{}) func(*fiber.Ctx) {
	return func(c *fiber.Ctx) {
		// ignore content-type and use json by default
		err := json.Unmarshal(c.Fasthttp.Request.Body(), obj)

		if err != nil {
			c.Status(http.StatusBadRequest)

			_ = c.JSON(ErrorResponse{
				Message: errors.HttpBodyMalformed,
				Details: err.Error(),
			})

			return
		}

		// validate fields
		err = obj.(Validator).Validate()

		if err != nil {
			c.Status(http.StatusBadRequest)

			_ = c.JSON(ErrorResponse{
				Message: errors.HttpBodyIsNotValid,
				Details: err.Error(),
			})

			return
		}

		c.Locals(BodyLocalsKey, obj)
		c.Next()
	}
}
