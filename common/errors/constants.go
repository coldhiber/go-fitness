package errors

const (
	HttpBodyMalformed       = "request body is malformed"
	HttpBodyIsNotValid      = "request body is not valid"
	HttpServerInternalError = "server internal error"
)
