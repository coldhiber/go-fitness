module go-fitness

go 1.14

require (
	cloud.google.com/go v0.60.0
	github.com/gofiber/fiber v1.12.2
	github.com/stretchr/testify v1.4.0
)
