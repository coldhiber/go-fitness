package errors

import "fmt"

type BookingCreationError struct {
	Message string
}

func (e *BookingCreationError) Error() string {
	return fmt.Sprintf("bookingcreation: %s", e.Message)
}
