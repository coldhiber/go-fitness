package bookings

import (
	"github.com/gofiber/fiber"
	"go-fitness/bookings/entities"
	bookingErrors "go-fitness/bookings/errors"
	"go-fitness/common"
	"go-fitness/common/errors"
	"go-fitness/storage"
	"net/http"
)

type BookingHandler struct {
	storage storage.Storage
}

func New(s storage.Storage) *BookingHandler {
	return &BookingHandler{
		storage: s,
	}
}

func (ch *BookingHandler) Create() func(c *fiber.Ctx) {
	return func(c *fiber.Ctx) {
		booking := c.Locals(common.BodyLocalsKey).(*entities.Booking)

		// perform save
		if err := ch.storage.SaveBooking(booking); err != nil {
			switch e := err.(type) {
			case *bookingErrors.BookingCreationError:
				c.Status(http.StatusConflict)

				_ = c.JSON(common.ErrorResponse{
					Message: e.Message,
				})
			default:
				c.Status(http.StatusInternalServerError)

				_ = c.JSON(common.ErrorResponse{
					Message: errors.HttpServerInternalError,
					Details: err.Error(),
				})
			}

			return
		}

		c.SendStatus(http.StatusCreated)
	}
}
