package storage

import (
	bookingEntities "go-fitness/bookings/entities"
	classEntities "go-fitness/classes/entities"
)

type Storage interface {
	SaveClass(class *classEntities.Class) error
	SaveBooking(booking *bookingEntities.Booking) error
}
