package storage

import (
	"cloud.google.com/go/civil"
	bookingEntities "go-fitness/bookings/entities"
	bookingErrors "go-fitness/bookings/errors"
	classEntities "go-fitness/classes/entities"
	classErrors "go-fitness/classes/errors"
	"sync"
)

type InMemory struct {
	classes  []*classEntities.Class
	bookings []*bookingEntities.Booking
	mutex    sync.Mutex
}

func New() *InMemory {
	return &InMemory{
		classes: []*classEntities.Class{},
	}
}

func (s *InMemory) SaveClass(class *classEntities.Class) error {
	// we think that we could have constraint in the db
	if class.StartDate.After(class.EndDate) {
		return &classErrors.ClassCreationError{Message: classErrors.EndDateBeforeStart}
	}

	// lock the list of classes
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// find intersection according the requirement one class per day
	for _, c := range s.classes {
		if checkClassesIntersection(c, class) {
			return &classErrors.ClassCreationError{Message: classErrors.BusySlots}
		}
	}

	s.classes = append(s.classes, class)

	return nil
}

func (s *InMemory) SaveBooking(b *bookingEntities.Booking) error {
	// lock the list of classes
	s.mutex.Lock()
	defer s.mutex.Unlock()

	for _, c := range s.classes {
		if exists := checkClassByDate(c, b.Date); exists {
			for _, b := range s.bookings {
				if b.Name == b.Name {
					return &bookingErrors.BookingCreationError{Message: bookingErrors.AlreadyBookedByThisPerson}
				}
			}

			s.bookings = append(s.bookings, b)
			return nil
		}
	}

	return &bookingErrors.BookingCreationError{Message: bookingErrors.NotAvailableClass}
}

func checkClassesIntersection(cOne, cTwo *classEntities.Class) bool {
	leftIntersection := checkClassByDate(cOne, cTwo.EndDate)
	rightIntersection := checkClassByDate(cOne, cTwo.StartDate)

	return leftIntersection || rightIntersection
}

func checkClassByDate(c *classEntities.Class, date civil.Date) bool {
	isWithinRange := c.StartDate.Before(date) && c.EndDate.After(date)
	borderCase := c.StartDate == date || c.EndDate == date

	return isWithinRange || borderCase
}
