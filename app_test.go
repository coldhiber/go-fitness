package main

import (
	"bytes"
	"github.com/gofiber/fiber"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"testing"
)

func TestNotAllowedClassMethods(t *testing.T) {
	tests := []testCase{
		{
			description:   "non existing route",
			route:         "/classes",
			method:        http.MethodGet,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non existing route",
			route:         "/classes",
			method:        http.MethodPut,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non existing route",
			route:         "/classes",
			method:        http.MethodDelete,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non existing route",
			route:         "/classes",
			method:        http.MethodPatch,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
	}

	testSuit(tests, t)
}

func TestCreatingClassWithWrongData(t *testing.T) {
	tests := []testCase{
		{
			description:   "creating a class without any data",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a class without dates",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\"}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a class with wrong startDate",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"start\": \"2006-01-02\", \"endDate\":  \"2006-01-02\"}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a class with end date before start date",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-01\"}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a class with zero capacity",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-04\",  \"capacity\":  0}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a class with negative capacity",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-04\",  \"capacity\":  -1}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
	}

	testSuit(tests, t)
}

func TestCreatingClass(t *testing.T) {
	tests := []testCase{
		{
			description:   "creating a class with valid data",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-04\",  \"capacity\":  1}",
			expectedError: false,
			expectedCode:  http.StatusCreated,
		},
	}

	testSuit(tests, t)
}

func TestNotAllowedBookingMethods(t *testing.T) {
	tests := []testCase{
		{
			description:   "non allowed route",
			route:         "/bookings",
			method:        http.MethodGet,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non allowed route",
			route:         "/bookings",
			method:        http.MethodPut,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non allowed route",
			route:         "/bookings",
			method:        http.MethodDelete,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
		{
			description:   "non allowed route",
			route:         "/bookings",
			method:        http.MethodPatch,
			expectedError: false,
			expectedCode:  http.StatusMethodNotAllowed,
		},
	}

	testSuit(tests, t)
}

func TestCreatingBookingWithWrongData(t *testing.T) {
	tests := []testCase{
		{
			description:   "creating a booking without any data",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a booking without date",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"John\"}",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
		{
			description:   "creating a booking with wrong date",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"John\", \"date\": \"-\" }",
			expectedError: false,
			expectedCode:  http.StatusBadRequest,
		},
	}

	testSuit(tests, t)
}

func TestCreatingBooking(t *testing.T) {
	tests := []testCase{
		{
			description:   "creating a class with valid data",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-04\",  \"capacity\":  1}",
			expectedError: false,
			expectedCode:  http.StatusCreated,
		},
		{
			description:   "creating a booking with valid data",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"John\", \"date\": \"2006-01-02\" }",
			expectedError: false,
			expectedCode:  http.StatusCreated,
		},
	}

	testSuit(tests, t)
}

func TestCreatingBookingIfClassIsAlreadyBooked(t *testing.T) {
	tests := []testCase{
		{
			description:   "creating a class with valid data",
			route:         "/classes",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"pilatos\", \"startDate\": \"2006-01-02\", \"endDate\":  \"2006-01-04\",  \"capacity\":  1}",
			expectedError: false,
			expectedCode:  http.StatusCreated,
		},
		{
			description:   "creating a booking with valid data",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"John\", \"date\": \"2006-01-02\" }",
			expectedError: false,
			expectedCode:  http.StatusCreated,
		},
		{
			description:   "creating a booking with valid data for already booked class",
			route:         "/bookings",
			method:        http.MethodPost,
			content:       "{ \"name\":  \"John\", \"date\": \"2006-01-02\" }",
			expectedError: false,
			expectedCode:  http.StatusConflict,
		},
	}

	testSuit(tests, t)
}

type testCase struct {
	description string

	// Test input
	route   string
	method  string
	content string

	// Expected output
	expectedError bool
	expectedCode  int
	expectedBody  string
}

func testRequest(test testCase, app *fiber.App, t *testing.T) {
	// Create a new http request with the route
	// from the test case
	req, _ := http.NewRequest(
		test.method,
		test.route,
		bytes.NewReader([]byte(test.content)),
	)

	// Perform the request plain with the app.
	// The -1 disables request latency.
	res, err := app.Test(req, -1)

	// verify that no error occured, that is not expected
	assert.Equalf(t, test.expectedError, err != nil, test.description)

	// As expected errors lead to broken responses, the next
	// test case needs to be processed
	if test.expectedError {
		return
	}

	// Verify if the status code is as expected
	assert.Equalf(t, test.expectedCode, res.StatusCode, test.description)

	// Read the response body
	body, err := ioutil.ReadAll(res.Body)

	// Reading the response body should work every time, such that
	// the err variable should be nil
	assert.Nilf(t, err, test.description)

	if test.expectedBody != "" {
		// Verify, that the response body equals the expected body
		assert.Equalf(t, test.expectedBody, string(body), test.description)
	}
}

func testSuit(tests []testCase, t *testing.T) {
	// Setup the app as it is done in the main function
	app := New()

	// Iterate through test single test cases
	for _, test := range tests {
		testRequest(test, app.server, t)
	}
}
